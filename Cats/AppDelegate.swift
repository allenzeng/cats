import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    // swiftlint:disable discouraged_optional_collection
    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool
    {
        let window = UIWindow(frame: UIScreen.main.bounds)
        let catsViewController = CatsViewController(service: CatsService(PeopleRequester()))
        let rootViewController = UINavigationController(rootViewController: catsViewController)
        window.rootViewController = rootViewController
        window.makeKeyAndVisible()

        self.window = window

        return true
    }
    // swiftlint:enable discouraged_optional_collection

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }

}
