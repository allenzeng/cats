import Alamofire

private let peopleURLString = "http://agl-developer-test.azurewebsites.net/people.json"

protocol Requester {
    func request() -> DataRequest
}

struct PeopleRequester: Requester {
    func request() -> DataRequest {
        return Alamofire.request(peopleURLString)
    }
}
