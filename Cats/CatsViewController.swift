import UIKit

import PromiseKit

private let loadingCellIdentifier = "loadingCellIdentifier"
private let textCellIdentifier = "textCellIdentifier"

class CatsViewController: UITableViewController {

    private let service: CatsService

    private weak var fetchOperation: Guarantee<Void>?

    private var viewModel: CatsViewModel = .loading {
        didSet {
            tableView.reloadData()
        }
    }

    init(service: CatsService) {
        self.service = service

        super.init(style: .plain)

        title = NSLocalizedString("Cats", comment: "Title of the view controller displaying all cats")
    }

    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let refreshControl = UIRefreshControl()
        tableView.refreshControl = refreshControl
        refreshControl.addTarget(self, action: #selector(fetchCats), for: .valueChanged)

        tableView.register(SpinnerCell.self, forCellReuseIdentifier: loadingCellIdentifier)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: textCellIdentifier)

        fetchCats()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRowsInSection(section)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch viewModel {
        case .loading:
            return dequeueReusableCell(withIdentifier: loadingCellIdentifier)
        case .error(let message):
            return textCell(withText: message)
        case .data(let sections):
            let catName = sections[indexPath.section].catNames[indexPath.row]
            return textCell(withText: catName)
        }
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.titleForHeaderInSection(section)
    }

    @objc
    private func fetchCats() {
        guard fetchOperation == nil else {
            return
        }

        fetchOperation = service.fetchCats()
            .done { [weak self] viewModel in
                self?.viewModel = viewModel
                self?.tableView.refreshControl?.endRefreshing()
            }
    }

    private func textCell(withText text: String) -> UITableViewCell {
        let cell = dequeueReusableCell(withIdentifier: textCellIdentifier)
        // swiftlint:disable force_unwrapping
        // if we don't have an actual cell text label we're in trouble
        cell.textLabel!.text = text
        // swiftlint:enable force_unwrapping
        return cell
    }

    private func dequeueReusableCell(withIdentifier identifier: String) -> UITableViewCell {
        // swiftlint:disable force_unwrapping
        // if we don't get an actual cell back we're in trouble
        return tableView.dequeueReusableCell(withIdentifier: identifier)!
        // swiftlint:enable force_unwrapping
    }

}
