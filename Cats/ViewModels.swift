enum CatsViewModel {
    case loading, error(String), data([CatsSectionViewModel])

    init(people: [Person]) {
        let filteredList = people
            .map { person -> (Gender, [String]) in
                let catNames = person.pets?
                    .filter { $0.type == .cat }
                    .map { $0.name }
                    .sorted(by: { first, second in
                        switch first.localizedCompare(second) {
                        case .orderedAscending:
                            return true
                        case .orderedDescending, .orderedSame:
                            return false
                        }
                    })
                    ?? []
                return (person.gender, catNames)
            }
            .filter { _, names in names.isEmpty == false }

        let viewModels = Dictionary(filteredList, uniquingKeysWith: +)
            .map { CatsSectionViewModel(gender: $0.rawValue, catNames: $1) }

        self = .data(viewModels)
    }

    var numberOfSections: Int {
        switch self {
        case .loading, .error:
            return 1
        case .data(let sections):
            return sections.count
        }
    }

    func numberOfRowsInSection(_ index: Int) -> Int {
        switch self {
        case .loading, .error:
            return 1
        case .data(let sections):
            return sections[index].catNames.count
        }
    }

    func titleForHeaderInSection(_ index: Int) -> String? {
        switch self {
        case .loading, .error:
            return nil
        case .data(let sections):
            return sections[index].gender
        }
    }
}

struct CatsSectionViewModel {
    let gender: String
    let catNames: [String]
}
