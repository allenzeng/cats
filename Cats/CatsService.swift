import PromiseKit

struct CatsService {
    private let peopleRequester: Requester

    init(_ peopleRequester: Requester) {
        self.peopleRequester = peopleRequester
    }

    func fetchCats() -> Guarantee<CatsViewModel> {
        return peopleRequester.request()
            .validate(statusCode: [200])
            .responseDecodable([Person].self)
            .map(CatsViewModel.init)
            .recover { error in
                let errorText: String
                switch error {
                case is DecodingError:
                    errorText = NSLocalizedString(
                        "Invalid data, please try again later",
                        comment: "The server returned a success response, but it contains invalid data")
                case AFError.responseValidationFailed:
                    errorText = NSLocalizedString(
                        "Server error, please try again later",
                        comment: "The server returned a state that isn't expected by the app")
                default:
                    errorText = NSLocalizedString("Unknown error.", comment: "Unknown error")
                }

                return Guarantee.value(.error(errorText))
            }
    }
}

struct Person: Decodable {
    let gender: Gender
    // swiftlint:disable discouraged_optional_collection
    let pets: [Pet]?
    // swiftlint:enable discouraged_optional_collection
}

struct Pet: Codable, Equatable {
    let type: Type
    let name: String

    enum `Type`: String, Codable, Equatable {
        case dog = "Dog", cat = "Cat", fish = "Fish"
    }
}

enum Gender: String, Decodable {
    case male = "Male", female = "Female"
}
