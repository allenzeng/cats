import XCTest

import Alamofire
import PromiseKit
import Swifter

@testable import Cats

// swiftlint:disable implicitly_unwrapped_optional
class CatsServiceTests: XCTestCase {
    private var server: HttpServer!

    private var mockRequester: MockPeopleRequester!

    deinit {
        server?.stop()
    }

    override func setUp() {
        super.setUp()

        if server != nil {
            return
        }

        let newServer = HttpServer()

        // swiftlint:disable force_try
        try! newServer.start()
        // swiftlint:enable force_try

        server = newServer

        mockRequester = MockPeopleRequester(newServer)
    }

    override func tearDown() {
        super.tearDown()

        server.stop()
    }

    func testHappyPath() {
        let validResponse = """
            [
                {
                    "gender": "Female",
                    "pets": [
                        {
                            "name": "Garfield",
                            "type": "Cat"
                        }
                    ]
                },
                {
                    "gender": "Male",
                    "pets": [
                        {
                            "name": "Tom",
                            "type": "Cat"
                        },
                        {
                            "name": "Max",
                            "type": "Cat"
                        },
                        {
                            "name": "Jim",
                            "type": "Cat"
                        }
                    ]
                }
            ]
            """
        server["/"] = { _ in .ok(.text(validResponse)) }

        let service = CatsService(mockRequester)

        runAsyncTest { expectation in
            service.fetchCats().done { viewModel in
                switch viewModel {
                case .error(let errorText):
                    XCTFail("test failed: \(errorText)")
                case .loading:
                    XCTFail("expecting view model state to be .data, but found .loading")
                case .data(let sectionViewModels):
                    XCTAssertEqual(sectionViewModels.count, 2)
                    XCTAssertEqual(sectionViewModels[0].gender, Gender.male.rawValue)
                    XCTAssertEqual(sectionViewModels[0].catNames, ["Jim", "Max", "Tom"])

                    XCTAssertEqual(sectionViewModels[1].gender, Gender.female.rawValue)
                    XCTAssertEqual(sectionViewModels[1].catNames, ["Garfield"])
                }

                expectation.fulfill()
            }
        }
    }

    func testNullPets_serializationSuccessful() {
        let validResponse = """
            [
                {
                    "gender": "Female",
                    "pets": [
                        {
                            "name": "Garfield",
                            "type": "Cat"
                        }
                    ]
                },
                {
                    "gender": "Male",
                    "pets": null
                }
            ]
            """
        server["/"] = { _ in .ok(.text(validResponse)) }

        let service = CatsService(mockRequester)

        runAsyncTest { expectation in
            service.fetchCats().done { viewModel in
                switch viewModel {
                case .error(let errorText):
                    XCTFail("test failed: \(errorText)")
                case .loading:
                    XCTFail("expecting view model state to be .data, but found .loading")
                case .data(let sectionViewModels):
                    XCTAssertEqual(sectionViewModels.count, 1)
                    XCTAssertEqual(sectionViewModels[0].gender, Gender.female.rawValue)
                    XCTAssertEqual(sectionViewModels[0].catNames, ["Garfield"])
                }

                expectation.fulfill()
            }
        }
    }

    func testExtraneousData_serializationSuccessful() {
        let validResponse = """
            [
                {
                    "someOtherField": "this shouldn't be a problem at all",
                    "gender": "Female",
                    "pets": [
                        {
                            "name": "Garfield",
                            "type": "Cat"
                        }
                    ]
                },
                {
                    "gender": "Male",
                    "pets": [
                        {
                            "weight": 3,
                            "name": "Tom",
                            "type": "Cat"
                        }
                    ]
                }
            ]
            """
        server["/"] = { _ in .ok(.text(validResponse)) }

        let service = CatsService(mockRequester)

        runAsyncTest { expectation in
            service.fetchCats().done { viewModel in
                switch viewModel {
                case .error(let errorText):
                    XCTFail("test failed: \(errorText)")
                case .loading:
                    XCTFail("expecting view model state to be .data, but found .loading")
                case .data(let sectionViewModels):
                    XCTAssertEqual(sectionViewModels.count, 2)
                    XCTAssertEqual(sectionViewModels[0].gender, Gender.male.rawValue)
                    XCTAssertEqual(sectionViewModels[0].catNames, ["Tom"])

                    XCTAssertEqual(sectionViewModels[1].gender, Gender.female.rawValue)
                    XCTAssertEqual(sectionViewModels[1].catNames, ["Garfield"])
                }

                expectation.fulfill()
            }
        }
    }

    func testInvalidData() {
        let invalidData = [
            "lkajdsfljldfkjaldsjf",
            "[jfkdjfaiej]",
            """
            [{
                "kjd": "j21nnf"
            }]
            """
        ]

        for invalidText in invalidData {
            server["/"] = { _ in .ok(.text(invalidText)) }

            let service = CatsService(mockRequester)

            runAsyncTest { expectation in
                service.fetchCats().done { viewModel in
                    switch viewModel {
                    case .error(let errorText):
                        let expectedText = NSLocalizedString(
                            "Invalid data, please try again later",
                            comment: "The server returns an invalid response")
                        XCTAssertEqual(expectedText, errorText)
                    case .loading, .data:
                        XCTFail("Unexpected state. Expecting `.error`")
                    }

                    expectation.fulfill()
                }
            }
        }
    }

    func testInvalidValues() {
        let template = """
            [
                {
                    "gender": "%@",
                    "pets": [
                        {
                            "name": "Tom",
                            "type": "%@"
                        }
                    ]
                }
            ]
            """
        let invalidValues = [
            ("this is clearly not a valid gender", "Cat"),
            ("Female", "not a valid pet type")
        ]
        for (genderRawValue, petTypeRawValue) in invalidValues {
            let invalidDataText = String(format: template, genderRawValue, petTypeRawValue)

            server["/"] = { _ in .ok(.text(invalidDataText)) }

            let service = CatsService(mockRequester)

            runAsyncTest { expectation in
                service.fetchCats().done { viewModel in
                    switch viewModel {
                    case .error(let errorText):
                        let expectedText = NSLocalizedString(
                            "Invalid data, please try again later",
                            comment: "The server returns an invalid response")
                        XCTAssertEqual(expectedText, errorText)
                    case .loading, .data:
                        XCTFail("Unexpected state. Expecting `.error`")
                    }

                    expectation.fulfill()
                }
            }
        }
    }

    func testServerErrors() {
        let serverErrors: [HttpResponse] = [
            .badRequest(nil),
            .unauthorized,
            .forbidden,
            .notFound,
            .internalServerError,
            .accepted,
            .created,
            .movedPermanently("")]

        for serverError in serverErrors {
            server["/"] = { _ in serverError }

            let service = CatsService(mockRequester)

            runAsyncTest { expectation in
                service.fetchCats().done { viewModel in
                    switch viewModel {
                    case .error(let errorText):
                        let expectedText = NSLocalizedString(
                            "Server error, please try again later",
                            comment: "The server returned a state that isn't expected by the app")
                        XCTAssertEqual(expectedText, errorText)
                    case .loading, .data:
                        XCTFail("Unexpected state. Expecting `.error`")
                    }

                    expectation.fulfill()
                }
            }
        }
    }

    private func runAsyncTest(_ runTest: @escaping (XCTestExpectation) -> Void) {
        let expectation = self.expectation(description: "")

        runTest(expectation)

        waitForExpectations(timeout: 1) { error in
            if let error = error {
                XCTFail("error during test: \(error)")
            }
        }
    }

    private struct MockPeopleRequester: Requester {
        private let server: HttpServer

        init(_ server: HttpServer) {
            self.server = server
        }

        func request() -> DataRequest {
            // swiftlint:disable force_try
            let port = try! server.port()
            // swiftlint:enable force_try
            return Alamofire.request("http://localhost:\(port)/")
        }
    }
}
// swiftlint:enable implicitly_unwrapped_optional
