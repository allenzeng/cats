### Running the project
This project uses CocoaPods. Running `pod install` and it whould be ready to go. Alternatively, [`bundler`](https://bundler.io) can be used too.

### Overview
#### Architecture
The view is driven by view models, which are produced by its dependent service. All logic in the view layer is purely for presentation, including the binding logic to apply view models.

This is nice because it allows us to write tests for the service and how it produces its view models. The implication is that inspecting the output view models is enough to cover all the business logic encapsulated. UI tests (not included in the project) can then be used to supplement aspects like navigation flow etc.

Downside is, the tests are potentially long and tedious to write. Also if the underlying state is complex, it may be difficult to test all permutations.

#### Unit tests
The tests on focused on the type `CatsService`, whose responsibility is to supply `CatsViewController` with all it needs to display its content. There is an in-memory server ([Swifter](https://github.com/httpswift/swifter)) that deals with the mock responses.

Obviously there is an infinite number of possibilities around invalid data, the tests cover some of these as an example.

#### Error handling
Some errors like invalid response codes and deserialisation failures are caught and tested. This could be expanded to cater for application specific domain errors etc. They can also be tested.
